package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_return_void() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_204_response_void() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/void-return"))
                .andExpect(MockMvcResultMatchers.status().is(204));
    }
}