package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_with_message_given_get_messages() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string("hello"));
    }

    @Test
    void should_return_200_with_object_message_given_get_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.value").value("hello"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    void should_return_202_with_object_message_given_get_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects-with-annotation/hello"))
                .andExpect(MockMvcResultMatchers.status().is(202))
                .andExpect(jsonPath("$.value").value("hello"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    void should_return_200_with_message_xth_header_given_get_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-entities/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.value").value("hello"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.header().string("X-Auth", "me"));
    }

//    @Test
//    void should_return_error_message_when_throw_exception() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/api/exception/runtime-exception"))
//                .andExpect(MockMvcResultMatchers.status().is(500));
//    }
}