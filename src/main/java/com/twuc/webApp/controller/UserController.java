package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping(value = "/api/no-return-value")
    public void noResult() {

    }

    @GetMapping(value = "/api/void-return")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void voidReturn() {

    }

}
