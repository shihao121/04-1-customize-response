package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @GetMapping(value = "/api/messages/{message}")
    public String getMessage(@PathVariable(value = "message") String message) {
        return message;
    }

    @GetMapping(value = "/api/message-objects/{message}")
    public Message getMessageObj(@PathVariable(value = "message") String message) {
        return new Message(message);
    }

    @GetMapping(value = "/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessages(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping(value = "/api/message-entities/{message}")
    public ResponseEntity<Message> getMessagesWithHeader(@PathVariable String message) {
        return ResponseEntity.ok().header("X-Auth", "me").body(new Message(message));
    }

    @GetMapping(value = "/api/exception/runtime-exception")
    public String throwError() {
        throw new RuntimeException("error");
    }

}
